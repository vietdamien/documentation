cmake_minimum_required(VERSION 3.15)
project(Documentation C)

set(CMAKE_C_STANDARD 99)

# Add all c source files under the src directory
file(GLOB SOURCES "src/*.c")
add_executable(${PROJECT_NAME} ${SOURCES})

# Add all headers files under the include directory
target_include_directories(${PROJECT_NAME} PRIVATE include)
