/* *****************************************************************************
 * Project name: Documentation
 * File name   : parser
 * Author      : Damien Nguyen
 * Date        : Wednesday, November 27 2019
 * ****************************************************************************/

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "io.h"
#include "parser.h"

bool isComment(const char *line) {
    return strlen(line) && line[1] == COMMENT_LINE_START;
}

bool isMacro(const char *line) {
    return strlen(line) && line[0] == MACRO_START;
}

bool isPrototype(const char *line) {
    return !isComment(line) && !isMacro(line)
           && strlen(line) && line[0] != INDENT_CHAR && line[0] != '}';
}

list_t getContent(const char *fileName) {
    char   tmp[STR_LEN];
    FILE   *file;
    list_t result = new();

    if (!(file = fopen(fileName, FILE_READ_MODE))) {
        fprintf(stderr, "getContent: fopen problem\n");
        exit(errno);
    }

    while (!feof(file) && readStr(tmp, STR_LEN, file)) {
        addLast(&result, tmp);
    }

    fclose(file);

    return result;
}

void printFilteredContent(list_t list, int argc, ...) {
    cell_t *current = list.head;

    while (current) {
        bool predicate = false;
        bool (*func)(const char *);
        int i;
        va_list args;
        va_start(args, argc);

        for (i = 0; i < argc; ++i) {
            func = va_arg(args, bool (*)(const char *));
            predicate |= (*func)(current->line);
        }
        if (predicate) { printf("%s\n", current->line); }

        current = current->next;
        va_end(args);
    }
}
