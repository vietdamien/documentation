/* *****************************************************************************
 * Project name: Documentation
 * File name   : list
 * Author      : Damien Nguyen
 * Date        : Wednesday, November 27 2019
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"

bool _initCell(cell_t **cell, char *line) {
    bool cellAllocated;

    if ((cellAllocated = (*cell = (cell_t *) malloc(sizeof(cell))))
        && ((*cell)->line = (char *) malloc(strlen(line) + 1))) {
        strcpy((*cell)->line, line);
        (*cell)->next = NULL;
    }

    return cellAllocated;
}

bool isEmpty(list_t list) {
    return list.head == NULL && list.tail == NULL;
}

list_t new(void) {
    list_t list = { NULL, NULL };
    return list;
}

void addFirst(list_t *list, char *line) {
    cell_t *new = NULL;

    if (!_initCell(&new, line)) {
        fprintf(stderr, "addFirst: malloc problem, aborting.\n");
        return;
    }

    if (!(new->next = list->head)) {
        list->tail = new;
    }

    list->head = new;
}

void addLast(list_t *list, char *line) {
    cell_t *new = NULL;

    if (!_initCell(&new, line)) {
        fprintf(stderr, "addLast: malloc problem, aborting.\n");
        return;
    }

    if (isEmpty(*list)) {
        list->head = new;
    } else {
        list->tail->next = new;
    }

    list->tail = new;
}

void removeFirst(list_t *list) {
    cell_t *tmp;
    if (isEmpty(*list)) { return; }

    tmp = list->head;
    list->head = list->head->next;

    if (!list->head) { list->tail = NULL; }

    free(tmp->line);
    free(tmp);
}

void clear(list_t *list) {
    while (!isEmpty(*list)) {
        removeFirst(list);
    }
}

void print(list_t list) {
    cell_t *current = list.head;

    while (current) {
        printf("%s\n", current->line);
        current = current->next;
    }
}
