/* *****************************************************************************
 * Project name: Documentation
 * File name   : main
 * Author      : Damien Nguyen
 * Date        : Wednesday, November 27 2019
 * ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "list.h"
#include "parser.h"

int main(int argc, const char **argv) {
    int status = EXIT_FAILURE;
    list_t list;

    if (argc != 2) {
        fprintf(stderr, "Usage: ./documentation <file_name>\n");
        goto Exit;
    }

    list = getContent(argv[1]);

    // print(list);
    printFilteredContent(list, 3, isPrototype, isComment, isMacro);
    clear(&list);

    status = EXIT_SUCCESS;
    Exit:
    return status;
}

void listTests() {
    int    i;
    char   tmp[128];
    list_t list = new();

    for (i = 0; i < 10; ++i) {
        sprintf(tmp, "Bonjour%d", i + 1);
        addLast(&list, tmp);
    }

    printf("FIRST: %s\n", list.head->line);
    printf("LAST: %s\n", list.tail->line);

    print(list);
    clear(&list);
}
