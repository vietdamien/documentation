/* *****************************************************************************
 * Project name: Documentation
 * File name   : io
 * Author      : Damien Nguyen
 * Date        : Wednesday, November 27 2019
 * ****************************************************************************/

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "io.h"

char *readLine(const char *const msg) {
    char   *line, tmp[STR_LEN];
    size_t len;

    printf(STR_PATTERN, msg);
    fflush(stdout);
    len = readStr(tmp, STR_LEN, stdin);

    if ((line = (char *) malloc((len * sizeof(char)))) == NULL) {
        perror("readLine: malloc.\n");
        exit(errno);
    }

    strcpy(line, tmp);
    return line;
}

double readNumber(const char *const msg) {
    char   *tmp;
    double number;

    tmp    = readLine(msg);
    number = strtod(tmp, NULL);

    if (!number && errno == EINVAL) {
        fprintf(stderr, "readNumber: failed to read the number.\n");
        free(tmp);
        exit(EXIT_FAILURE);
    }

    free(tmp);
    return number;
}

size_t readStr(char *dst, size_t len, FILE *in) {
    size_t read = 0;

    if (fgets(dst, len, in) && dst[(read = strlen(dst)) - 1] == CHAR_NEW_LINE) {
        dst[read - 1] = CHAR_END_OF_STRING;
    }

    return read;
}
