/* *****************************************************************************
 * Project name: Documentation
 * File name   : parser
 * Author      : Damien Nguyen
 * Date        : Wednesday, November 27 2019
 * ****************************************************************************/

#ifndef DOCUMENTATION_PARSER_H
#define DOCUMENTATION_PARSER_H

#include <stdbool.h>

#include "list.h"

// ========================== CONSTANTS AND MACROS ========================== //
#define COMMENT_LINE_START '*'
#define INDENT_CHAR        ' '
#define MACRO_START        '#'

// ============================== DECLARATIONS ============================== //
bool isComment(const char *line);
bool isMacro(const char *line);
bool isPrototype(const char *line);

list_t getContent(const char *fileName);

void printFilteredContent(list_t list, int argc, ...);

#endif // DOCUMENTATION_PARSER_H
