/* *****************************************************************************
 * Project name: Documentation
 * File name   : io
 * Author      : Damien Nguyen
 * Date        : Wednesday, November 27 2019
 * ****************************************************************************/

#ifndef DOCUMENTATION_IO_H
#define DOCUMENTATION_IO_H

#include <stdio.h>

// ========================== CONSTANTS AND MACROS ========================== //
#define STR_LEN 1024

#define CHAR_END_OF_STRING '\0'
#define CHAR_NEW_LINE      '\n'

#define FILE_READ_MODE "r"

#define ESCAPE_PATTERN "%*c"
#define STR_PATTERN    "%s"

// ============================== DECLARATIONS ============================== //
/**
 * Prompts the user to input a string.
 *
 * @param msg the prompt message
 * @return the user's input string
 */
char *readLine(const char *msg);

/**
 * Prompts the user to input a number (all verifications are done in this
 * function.
 *
 * The result will have to be cast if needed.
 *
 * @param msg the prompt message
 * @return the user's input number
 */
double readNumber(const char *msg);

/**
 * Reads a string with the given maximum length of characters from the given
 * input file and stores it into the destination character array.
 *
 * @param dst the destination character array
 * @param len the given maximum length
 * @param in the given input file
 * @return the length of the actually read string INCLUDING the '\n'
 */
size_t readStr(char *dst, size_t len, FILE *in);

#endif // DOCUMENTATION_IO_H
