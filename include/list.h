/* *****************************************************************************
 * Project name: Documentation
 * File name   : list
 * Author      : Damien Nguyen
 * Date        : Wednesday, November 27 2019
 * ****************************************************************************/

#ifndef DOCUMENTATION_LIST_H
#define DOCUMENTATION_LIST_H

#include <stdbool.h>

// ========================== CONSTANTS AND MACROS ========================== //


// =============================== STRUCTURES =============================== //
typedef struct cell {
    char *line;
    struct cell *next;
} cell_t;

typedef struct {
    cell_t *head;
    cell_t *tail;
} list_t;

// ============================== DECLARATIONS ============================== //
bool isEmpty(list_t list);

list_t new(void);

void addFirst(list_t *list, char *line);
void addLast(list_t *list, char *line);
void removeFirst(list_t *list);
void clear(list_t *list);
void print(list_t list);

void listTests();

#endif // DOCUMENTATION_LIST_H
